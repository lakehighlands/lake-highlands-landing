Welcome to Lake Highlands Landing Beautifully landscaped grounds surround the quiet community of Lake Highlands Landing. Located on Forest Lane just off I-635 in Dallas, Texas, our community offers you an easy, convenient, and comfortable way to live green.

Address: 9750 Forest Lane, Dallas, TX 75243, USA

Phone: 972-885-3428

Website: http://www.lakehighlandslandingapartments.com
